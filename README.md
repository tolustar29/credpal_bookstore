## CredPal BookStore

- This project is a Web Application built using Vue.js (Frontend) and Laravel (Backend).

## Features
- All users (Visitor, signed in user, admin) can view all book details
- All users have the ability to sort and filter books based on title or average review
- All users have the ability to search books by title or author ID
- Only signed in users can leave a review and comment
- Only admin can store a book in the database

## Get Started
- Get started by cloning this repository into your local computer by running "git clone [https://gitlab.com/tolustar29/credpal_bookstore.git](https://gitlab.com/tolustar29/credpal_bookstore.git)" in terminal and "CD" into the directory of the cloned repository on your computer
- Open the cloned repository on your local computer then run "cp .env.example .env && php artisan key:generate" in terminal and fill in your environment variables, ESPECIALLY YOUR DATABASE ENVIRONMENT
- Type 'composer install' on your terminal and press enter
- Once all packages has been installed, type 'php artisan migrate' to migrate all tables to your database
- Then install passport keys by typing 'php artisan passport:install' in the terminal and enter
- Run "composer dump-autoload" and afterwards run "php artisan db:seed" 
- Then run "php artisan serve" to start the application

- ***Note 1: You can run test by typing "vendor/bin/phpunit" in terminal to ensure the application is working perfectly
- ***Note 2: Testing apis with admin authorization, you can use this email and password - 
        ---Email: admin@credpal.com
        ---Password: password 
        *-or you can change any user role from "user" to "admin"


## Developer
Olaogun Daniel Toluwalase 

[https://tolustar.com](https://tolustar.com)

08039144365

[https://www.linkedin.com/in/olaogun-daniel-toluwalase-473059b0/](https://www.linkedin.com/in/olaogun-daniel-toluwalase-473059b0/)
