<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\BookReviewResource;

class BookReviewController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if(!Book::find($id)){
            return response()->json([
                "errors" => "Book can not be found",
            ], 404);
        }

        $validator = Validator::make($request->all(), [
            'review' => 'required|integer',
            'comment' => 'required|string',
        ]);

        if ($validator->fails() ) {
            return response()->json([
                "errors" => $validator->messages(),
            ], 422);
        }

        $book_review = BookReview::create([
            'user_id' => auth()->id(),
            'book_id' => $id,
            'review' => $request->get('review'),
            'comment' => $request->get('comment'),
        ]);

        return new BookReviewResource($book_review);
    }

}
