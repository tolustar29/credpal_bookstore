<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\BookResource;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sortColumn = 'title'; $sortDirection = 'ASC';

        if(request()->get('sortColumn') != null){
            $sortColumn = request()->get('sortColumn');
        }

        if(request()->get('sortDirection') != null){
            $sortDirection = request()->get('sortDirection');
        }
        
        $books = Book::withCount(['reviews as avg_review' => function($query) {
            $query->select(\DB::raw('coalesce(avg(review),0)'));
        }])->orderBy($sortColumn, $sortDirection)->where("title","LIKE","%".request()->get('title')."%");

        $author_ids = request()->get('author_id');
        if($author_ids  != null){
            $books = $books->whereIn('author_id', explode(',',$author_ids));
        }

        $books = $books->paginate(15);

        return BookResource::collection($books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'isbn' => 'required|unique:books,isbn|min:13',
            'author_id' => 'required|exists:authors,id',
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->messages(),
            ], 422);
        }

        $book = Book::create([
            'author_id' => $request->get('author_id'),
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'isbn' => $request->get('isbn'),
        ]);

        return new BookResource($book);
    }

}
