<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(){

        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required|min:5',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->messages(),
            ], 422);
        }

        if(auth()->attempt([
            "email" => request()->get('email'),
            "password" => request()->get('password'),
        ])){
            $user = User::where("email", request()->get('email'))->first();
            $token = $user->createToken('credpal')->accessToken;
        }
        
        return response()->json(compact("token"));
        
    }

    public function register(){

        $validator = Validator::make(request()->all(), [
            'email' => 'required|unique:users,email',
            'name' => 'required',
            'password' => 'required|min:5',
            'role' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "errors" => $validator->messages(),
            ], 422);
        }

        $user = new User;
        $user->name = request()->get('name');
        $user->email = request()->get('email');
        $user->password = bcrypt(request()->get('password'));
        $user->role = request()->get('role');
        $user->save();

        return response()->json(['Registration successful. Please login !!!']);
        
    }

}
