<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'author_id' => $this->author_id,
            'isbn' => $this->isbn,
            'title' => $this->title,
            'description' => $this->description,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
            'authors' => [
                'id' => $this->author->id,
                'name' => $this->author->name,
            ],
            'reviews' => [
                'avg' => (int) $this->reviews->avg('review'),
                'count' => (int) $this->reviews->count(),
            ],
        ];

    }
}
