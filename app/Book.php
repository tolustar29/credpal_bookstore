<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = ['author_id','isbn', 'title', 'description'];
    
    public function reviews()
    {
        return $this->hasMany('App\BookReview');
    }

    public function author()
    {
        return $this->belongsTo('App\Author');
    }
    
}
