<?php

namespace Tests\Feature;

use App\Book;
use App\User;
use App\Author;
use App\BookReview;
use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_add_a_book_to_database()
    {
        Passport::actingAs(
            factory(User::class)->create([
                "email" => "user@credpal.com",
                "role" => "admin"
            ])
        );

        factory(User::class)->create();
        factory(Author::class, 10)->create();

        $response = $this->post('/api/books', [
            "isbn" => "1234567891011",
            "author_id" => 1,
            "title" => 'The Pelican Brief',
            "description" => "Detective novel",
        ]);

        $response->assertStatus(201)->assertJson([
            'data' => true,
        ]);
    }

    /** @test */
    public function anyone_can_view_all_books_in_the_database()
    {
    
        factory(User::class,10)->create();
        factory(Author::class, 10)->create();
        factory(Book::class, 40)
        ->create()
        ->each(function ($review) {
            $review->reviews()->createMany(
                factory(BookReview::class, 15)->make()->toArray()
            );
        });

        $response = $this->get('/api/books');

        $response->assertStatus(200)->assertJson([
            'data' => true,
        ])->assertJsonCount(15, 'data');
    }
}
