<?php

namespace Tests\Feature;

use App\Book;
use App\User;
use App\Author;
use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookReviewTest extends TestCase
{
    
    use RefreshDatabase;

    /** @test */
    public function authenticated_user_can_post_book_review()
    {
        Passport::actingAs(
            factory(User::class)->create([
                "email" => "user@credpal.com",
            ])
        );

        factory(User::class)->create();
        factory(Author::class, 10)->create();
        factory(Book::class, 40)->create();

        $response = $this->post('/api/books/1/reviews', [
            "user_id" => 1,
            "author_id" => 1,
            "review" => 5,
            "comment" => "The book is awesome",
        ]);

        $response->assertStatus(201)->assertJson([
            'data' => true
        ]);
    }
}
