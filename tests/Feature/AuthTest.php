<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Laravel\Passport\Passport;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function user_can_register()
    {
        $response = $this->post('/api/register', [
            "name" => "user",
            "email" => "user@credpal.com",
            "password" => "password",
            'role' => 'user',
        ]);

        $response->assertStatus(200)->assertJson([
            'Registration successful. Please login !!!'
        ]);
    }

    /** @test */
    public function user_can_not_visit_apis_restricted_to_admin()
    {
        
        Passport::actingAs(
            factory(User::class)->create([
                "email" => "user@credpal.com",
            ])
        );
        
        $response = $this->post('/api/books', [
            "isbn" => "1234567896321",
            "author_id" => "1",
            "title" => "The Lawyer",
            "description" => "Newyork best selling",
        ]);

        $response->assertStatus(401)->assertJson([
            'You have no authorization'
        ]);
    }

    /** @test */
    public function unauthorized_user_cannot_visit_protected_apis()
    {
        
        $response = $this->post('/api/books/1/reviews', [
            "review" => 5,
            "comment" => "The book is awesome",
        ]);
  
        $response->assertStatus(500);
    }

}
