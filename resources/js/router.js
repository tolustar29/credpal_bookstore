import Vue from "vue";
import VueRouter from "vue-router";

import Home from "./components/Home";
import Books from "./components/Books";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/books",
    name: "books",
    component: Books
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior(to) {
    if (to.query.page == null) {
      return {
        x: 0,
        y: 0
      };
    }
  },
  routes
});

export default router;
