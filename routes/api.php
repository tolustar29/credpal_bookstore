<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', "AuthController@login");

Route::post('register', "AuthController@register");

Route::get('books', "BookController@index");

Route::group(['middleware' => ['auth:api']], function () {

    Route::post('books/{id}/reviews', "BookReviewController@store");

});

Route::group(['middleware' => ['auth:api', 'auth.admin']], function () {

    Route::post('books', "BookController@store");
    
});