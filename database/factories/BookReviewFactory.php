<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BookReview;
use Faker\Generator as Faker;

$factory->define(BookReview::class, function (Faker $faker) {
    return [
        'book_id' => mt_rand(1,20),
        'user_id' => mt_rand(1,3),
        'review' => mt_rand(1,10),
        'comment' => $faker->text,
    ];
});
