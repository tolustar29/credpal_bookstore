<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Book::class, 40)
        ->create()
        ->each(function ($review) {
            $review->reviews()->createMany(
                factory(App\BookReview::class, 15)->make()->toArray()
            );
        });
    }
}
