<?php

use Illuminate\Database\Seeder;

class BookReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BookReview::class, 50)->create();
    }
}
