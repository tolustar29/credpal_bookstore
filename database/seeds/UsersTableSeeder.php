<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            "role" => "admin",
            "name" => "credpal",
            "email" => "admin@credpal.com",
        ]);
        factory(App\User::class, 5)->create();
    }
}
